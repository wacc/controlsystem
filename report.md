# WCC Control System Report
######Group 1:
###### Jasper van Tuijl
###### Daan Krol 
###### Jeroen van Brandenburg



## Introduction

In this report we discuss our implementation and design choices of our project of the course Web and Cloud 
Computing. We created a heating and ventilation control system, which combines information of simulated sensors with
real weather data, to activate simulated actuators. Our system can be deployed on any server with 
sufficient hardware and Node, Npm, Docker and Docker-Compose installed. It is also easily scalable when Kubernetes 
is installed.

## Architecture
### NodeJS

[//]: # (what, why)
Since we all have some experience using JavaScript, we decided to use the NodeJs environment to build our 
application. With Node, we can write both the backend and frontend in Javascript which makes the development cycle 
 easier for us since we all have experience with JavaScript and thus we do not have to find another language for the 
 backend. There are many easy to use package managers (npm, yarn), which allows us to use external packages. These 
package managers are especially useful when using dockerization, since they list all dependencies that should be 
acquired.

[//]: # (how)


### Docker

[//]: # (what, why)
We use Docker to encapsulate every part of the system. This ensures that each part can be run independently on every 
 platform. Decreasing platform specific problems and dependency issues. Docker makes it really easy to test and 
 deploy our application in our CI/CI pipeline. We can tag new development builds so that we can both debug new 
 code while maintaining a working production version. This enables us to split up the development. One team member can 
 develop on the frontend while using a stable backend build while another can safely change a development build of the 
 backend. Docker also simplifies the deployment on Kubernetes. For each deployment configuration we can specify which
 docker image (and specifically which tag) we want to use. Whenever a pod is deleted, it fetches the latest version of 
 that tag.  
   
[//]: # (how)
For every component, we made a separate Docker. Therefore, we created a unique Dockerfile for the Frontend, Backend, 
Device and Influxdb. We created a docker-compose file to run instances of all dockers at one time. For the production 
version of the system we made special Dockerfile's so that the package is build in minified.
 An additional docker-compose file is made for testing the backend. Since for example the frontend is not necessary to
  test the backend.
This greatly decreases build times in the Gitlab CI since the frontend contains over 1600+ packages. This also helps to 
 reduce the usage of our free Gitlab CI minutes. 
 
### Kubernetes

[//]: # (what, why)
To mange and run multiple versions of our docker images, we use Kubernetes. Since our deployment is connected to our git
repository, new pods are automatically run with the latest version. 

[//]: # (how)

## Frontend
### Vue


### NuxtJS

[//]: # (what, why)
We chose NuxtJS to create the frontend of our web application. We all were familliar with the framework VueJS. NuxtJS
expends this framework, which makes creating apps even easier. For example, it has a better default project structure 
and simplifies routing through different pages.

[//]: # (how)
Our application consists of 6 different pages: Login, Index (home screen), Devices, Status, Users and Datastream. A user
starts at the Login screen, and after logging in, is forwarded to the Index. The other pages can be reached using the 
menu on the left side.

### Auth/JWT

[//]: # (what, why)
JSON Web Tokens are tokens which can be generated, and can be granted to users, to allow/enable secure usage of the web 
application. They are an industry standard, and work convenient in NodeJS.

[//]: # (how)
When a user is registered in the user MongoDB, he can log in. He receives an JWT which will enable the user to send
requests to the application for [TODO 15min].

### Axios

[//]: # (what, why)
We use Axios to send requests to different components of our application. It can be easily used in JS to send HTTP 
requests, and automates transformation for JSON data.

[//]: # (how)
Axios is mainly used in the frontend to access the backend through API requests. It is also used in the backend itself, 
to obtain the current weather predictions of openweathermap.org. We also use Axios in the CI, when performing some 
initialization for functionality tests.

### Bootstrap-Vue

[//]: # (what, why)
Bootstrap provides us with many basic page elements, with a nice looking style. Its grid system facilitates the design
layout of our webpages. This also reduces the amount of CSS we need to use.

[//]: # (how)
We use different Bootstrap components in our application. For example, the cards, buttons, and the many different 
tables throughout the project. 

### SocketIO

[//]: # (what, why)
We use SocketIO as a websocket server to receive sensor data fast and continuously on the backend.

[//]: # (how)
In JSON formats, the data of new readings of the sensors is send to the backend.
## Backend
### MongoDB

[//]: # (what, why)
MongoDB is a non-SQL database, which uses JSON to describe its objects. 

TODO: it is strong consistent ?:https://stackoverflow.com/questions/11292215/where-does-mongodb-stand-in-the-cap-theorem

[//]: # (how)
We use MongoDB to keep track of the information of different users. It keeps track of the username, email, password and 
JWT token. We also have a db that keeps track of the devices. It stores the Device name, type, subtype, status and
whether is it favorite or not.

### InfluxDB

[//]: # (what, why)
We use InfluxDB to keep track of the sensor data. We chose influxDB, because it is a time series database. This way
groups of data can quickly be retrieved within a certain time interval.

[//]: # (how)
All the sensor data is stored in Influxdb, with a sensor id, type and subtype. 

### Mosquitto

[//]: # (what, why)
To enable communication between the devices (sensors, actuators) and the backend, we implemented communication with the 
message protocol MQTT. MQTT is often used for small microcontrollers, as it is designed for the IoT. As the MQTT broker, 
we decided to use Mosquitto, since it is very lightweight.

[//]: # (how)
The MQTT broker on the backend listens if there are new sensors, and if there are new sensor readings. These new sensors
are stored in the MongoDB and the sensors readings in the InfluxDB.

### ExpressJS

[//]: # (what, why)
ExpressJs is the most famous NodeJS framework. Its flexible, minimal and is easy scalable. 

[//]: # (how)
We use ExpressJS to handle the communication on the backend. Using routers, we created a REST api, to make the backend
accessible for the frontend. 

## Devices

In the system external devices such as sensors for temperature and humidity or actuators such as air conditioning and
heaters are controlled and monitored. Sensor readings are visualised on the dashboard of the frontend including but also
the state of an actuator so that the user knows if his heater is on or off. The user can manually change the 
state of each actuator.
 
In this project we simulate such devices by running them in a docker container running a Node instance. All communication
between the device and the backend goes through a MQTT message broker. The power of MQTT is that the developer is free
to chose what payload to send to a topic. We use a bidirectional Remote Procedure Call (RPC) mechanism over MQTT.
In essence the MQTT topic structure is used as transport for the request and response strategy.

A device can be of type actuator or sensor. Actuators can be of the subtype Heater or AC. Sensors can
be of the subtype humidity or temperature. Type and subtype have to be specified on construction of the device.  
On construction the device will keep calling the RPC on the backend to register itself as a new device and to get an ID.
Upon receiving an ID the device itself registers two RPC calls. One getter for his own state and one RPC for toggling 
this state remotely. If the device is a sensor it will send a random numeric value to the backend every 20 seconds by 
calling the RPC of the backend. 

## Deployment
### Ingress

[//]: # (what, why)

[//]: # (how)

### Environment files

[//]: # (what, why)
When building the application with docker-compose, the developer can pass along environment variables. These variables, 
such as URIs and ports, enable the different components to find each other over the shared network.

[//]: # (how)
In our environment file, we define connection parameters for the MongoDB and InfluxDB. We also declare variables for the
frontend configuration and state the port on which the api will be served.

## CI
### GitLab

[//]: # (what, why)
GitLab is a development platform with an easy-to-use Web IDE. It covers the whole development and operations cycle.
It has build in continuous integration and delivery features, which are convenient when pushing updates while
developing. 

[//]: # (how)

### Mocha

[//]: # (what, why)
Mocha is a simple and flexible javascript framework. It serially runs tests to audit functionality.

[//]: # (how)
Whenever we push updates to gitlab, we test if the backend behaves as it is supposed to. To check this, we wrote a
series of test files, which are divided by the used routes. To perform the assertions, we use the Chai assertion library. 

