import socket from '~/plugins/socket.io.js'

export const state = () => ({
    list: [],
    init: false
})

export const mutations = {
    setDevices(state, devices) {
        state.list = devices
        state.init = true
    },
    removeDevice(state, _id) {
        let index = state.list.find( x => x._id === _id)
        state.list.splice(state.list.indexOf(index), 1)
    },
    toggleFavorite(state, device) {
        device.favorite = !device.favorite;
    },
    addDevice(state, device){
        state.list.push(device)
    },
    setDeviceValue(state, update) {
        state.list.forEach((device, index) => {
            if (device._id === update._id) {
                this._vm.$set(device, 'value', update.value)
            }
        })
    },
    setDeviceStatus(state, update) {
        state.list.forEach((device, index) => {
            if (device._id === update._id) {
                this._vm.$set(device, 'status', update.status)
            }
        })
    },
    setDeviceName(state, update) {
        state.list.forEach((device, index) => {
            if (device._id === update._id) {
                this._vm.$set(device, 'name', update.name)
            }
        })
    },
    toggleStatus(state, device) {
        device.status = !device.status
    }
}

// Actions are used for asynchronous activity.
export const actions = {
    async get({commit}) {
        await this.$axios.get('/devices')
            .then(res => {
                commit('setDevices', res.data.devices)
            })
            .catch(err => console.error(`store cannot fetch devices from api: ${err}`))
    },
    async remove({commit}, _id) {
        commit('removeDevice', _id)
    },
    async toggleFavorite({commit}, device) {
        await this.$axios.post('/devices/update',
            {_id: device._id, favorite: !device.favorite})
            .then(() => {
                commit('toggleFavorite', device)
            })
            .catch(err => console.error(`Could not toggle favorite: ${err}`))
    },
    async toggleStatus({commit}, device) {
        await this.$axios.post('/devices/toggleStatus',{_id: device._id})
            .then((response)=> {
                commit('toggleStatus', device)
            })
    },
    async setDeviceStatus({commit}, payload) {
        commit('setDeviceStatus', payload)
    },
    async setDeviceValue({commit}, payload) {
        commit('setDeviceValue', payload)
    },
    async setDeviceName({commit}, payload) {
        await this.$axios.post('/devices/update/name', {_id: payload._id, name: payload.name})
            .then(()=> commit('setDeviceName', payload))

    }
}

export const getters = {
    favorites: state => {
        return state.list.filter(device => device.favorite)
    },
    favoriteActuators: state => {
        return state.list.filter(device => device.favorite && device.type === 'Actuator')
    },
    favoriteSensors: state => {
        return state.list.filter(device => device.favorite && device.type === 'Sensor')
    }
}