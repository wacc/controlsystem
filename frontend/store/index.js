export const state = () => ({
    backendOnline: true
})

export const mutations = {
    setBackendOnline(state, status){
        state.backendOnline = status
    },
}


export const actions = {
    setBackendOnline({commit}, status) {
        commit('setBackendOnline', status)
    }
}

