import { Line, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins
import * as ChartAnnotation from 'chartjs-plugin-annotation'

export default {
  extends: Line,
  mixins: [reactiveProp],
  props: ['options'],
  mounted () {
    // this.chartData is created in the mixin.
    // If you want to pass options please create a local options object
    this.addPlugin({
      ChartAnnotation
    })
    this.renderChart(this.chartData, this.options)
  }
}
