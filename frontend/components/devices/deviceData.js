export default [
  {
    name: 'sensor1',
    type: 'Sensor',
    subtype: 'Temperature Outside',
    status: true,
    value: 44,
    favorite: false
  },
  {
    name: 'sensor2',
    type: 'Sensor',
    subtype: 'Temperature Inside',
    status: true,
    value: 22,
    favorite: true
  },
  {
    name: 'actuator1',
    type: 'Actuator',
    subtype: 'Heater',
    status: true,
    favorite: true
  },
  {
    name: 'actuator2',
    type: 'Actuator',
    subtype: 'AC',
    status: false,
    favorite: false
  }
]
