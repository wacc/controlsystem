export default [
  {
    url: '/',
    name: 'Home',
    slug: 'home',
    icon: 'house-fill'
  },
  {
    url: '/devices',
    name: 'Devices',
    slug: 'devices',
    icon: 'thermometer-half'
  },
  {
    url: '/users',
    name: 'Users',
    slug: 'users',
    icon: 'person-fill'
  },
  {
    url: '/status',
    name: 'Status',
    slug: 'status',
    icon: 'broadcast-pin'
  }
]
