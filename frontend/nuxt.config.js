
export default {
    /*
     ** Nuxt rendering mode
     ** See https://nuxtjs.org/api/configuration-mode
     */
    ssr: true,
    /*
     ** Headers of the page
     ** See https://nuxtjs.org/api/configuration-head
     */
    head: {
        title: process.env.npm_package_name || '',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },

    /*
     ** Global CSS
     */
    css: [
    ],

    /*
     ** Plugins to load before mounting the App
     ** https://nuxtjs.org/guide/plugins
     */
    plugins: [
        // {src: '~/plugins/socket.io.js'}
    ],

    /*
     ** Auto import components
     ** See https://nuxtjs.org/api/configuration-components
     */
    components: true,

    dev: process.env.NODE_ENV !== 'production',

    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [
        // Doc: https://github.com/nuxt-community/eslint-module
        '@nuxtjs/eslint-module'
    ],

    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://http.nuxtjs.org
        '@nuxtjs/axios',
        '@nuxtjs/auth',
        'bootstrap-vue/nuxt',
    ],
    env: {
        BACKEND_HOST: process.env.BACKEND_HOST,
        API_ENDPOINT: process.env.API_ENDPOINT,
        SOCKET: process.env.SOCKET,
    },

    axios: {
        baseURL: process.env.BACKEND_HOST + process.env.API_ENDPOINT 
    },

    bootstrapVue: {
        icons: true // Install the IconsPlugin (in addition to BootStrapVue plugin
    },

    /*
     ** Authentication options
     */
    auth: {
        strategies: {
            local: {
                endpoints: {
                    login: { url: process.env.BACKEND_HOST + process.env.API_ENDPOINT + '/auth/login', method: 'post', propertyName: 'token' },
                    logout: { url: process.env.BACKEND_HOST + process.env.API_ENDPOINT + '/auth/logout', method: 'post' },
                    user: { url:  process.env.BACKEND_HOST + process.env.API_ENDPOINT + '/auth/user', method: 'get', propertyName: 'user' }
                }
            }
        }
    }

}
