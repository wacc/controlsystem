import io from 'socket.io-client'
const socket = io(process.env.BACKEND_HOST + process.env.SOCKET)
export default socket
