# WCC Control System Report
###### Group 1:
###### Jasper van Thuijl (s2503204)
###### Daan Krol (s3142221)
###### Jeroen van Brandenburg (s3193063)


## Introduction

In this report, we discuss our implementation and design choices of our project of the course Web and Cloud Computing. We created a heating and ventilation control system, which combines information of simulated sensors with real weather data, to activate simulated actuators. Our system can be deployed on any server with sufficient hardware and Node, Npm, Docker and Docker-Compose installed. It is also easily scalable when Kubernetes is installed. A status page gives an overview of the Kubernetes deployments, events and pods. It also allows you to quickly scale the deployments up or down and you can manually kill pods.

## Architecture
### NodeJS

[//]: # (what, why)
Since we all have some experience using JavaScript, we decided to use the NodeJs environment to build our application. With Node, we can write both the backend and frontend in Javascript which makes the development cycle  easier for us since we all have experience with JavaScript and thus we do not have to find another language for the  backend. There are many easy to use package managers (npm, yarn), which allows us to use external packages. These package managers are especially useful when using dockerization since they list all dependencies that should be acquired.

[//]: # (how)


### Docker

[//]: # (what, why)
We use Docker to encapsulate every part of the system. This ensures that each part can be run independently on every  platform. It also decreases platform-specific problems and dependency issues. Docker makes it really easy to test and deploy our application in our CI/CI pipeline. We can tag new development builds so that we can both debug new code while maintaining a working production version. This enables us to split up the development. One team member can develop on the frontend while using a stable backend build, while another can safely change a development build of the backend. Docker also simplifies the deployment on Kubernetes. For each deployment configuration, we can specify which docker image (and specifically which tag) we want to use. Whenever a pod is deleted, it fetches the latest version of that tag.  
   
[//]: # (how)
For every component, we made a separate Docker. We created a unique Dockerfile for the Frontend, Backend, Device and Influxdb. We created a docker-compose file to run instances of all dockers at one time. For the production version of the system, we made special Dockerfiles so that the package is built and minified. An additional docker-compose file is made for testing the backend. The frontend, for example, is not necessary to be able to test the backend. This greatly decreases build times in the Gitlab CI since the frontend contains over 1600+ packages. This also helps to reduce the usage of our limited free Gitlab CI minutes.

### Visual represtation architecture
![](https://i.imgur.com/Domt7Qq.png)

 
### Kubernetes
[//]: # (what, why)
As container orchestration system we use Kubernetes, which is responsible for the deployment, scaling and management of containerized applications. Next to this, Kubernetes also performs monitoring and logging, and can also be used to easily roll out deployments. Since the Kubernetes deployment configuration files are located in GitHub, and the docker images with the latest versions of our backend and frontend are located in our private docker hub repository, our project can be easily rolled out. For this reason, migrating between Kubernetes clusters (for example AWS or ) should also be easily feasible.
When the demand is high, Kubernetes can automatically scale up by deploying more pods, even over different servers. When the demand is low, it can also scale down. Whenever a pod fails or crashes, Kubernetes can quickly deploy a new pod.
Another benefit of Kubernetes is that it allows communication between the different pods in a cluster.

[//]: # (how)
To set up Kubernetes, we used microk8s, which can be easily set up on Ubuntu. While we originally planned to set up an infrastructure of at least 3 nodes, we only used a single node in the end, due to internal network errors on our Microk8s cluster, which occurred frequently after running the cluster for a certain time.

All our deployed docker containers are managed by Kubernetes. In our configuration files, we have set the number of replicas for the backend, MongoDB and the frontend to 3. In these files, we also define services which load balance requests over the pods. This will be discussed in more detail in the following subsection.

Internally, services can be accessed through the internal DNS. For example, to communicate from one of the backend pods to the MongoDB database, one can simply pass `mongo-0.mongodb-headless.default.svc.cluster.local`. 


Kubernetes' convenient API, made it possible for us to create a status panel in our frontend, to manage all the running pods. We even implemented functionality that allows us to scale up/down, using the user interface.


### Infrastructure
For this project, we chose to use instances based on the Ubuntu 20.04 LTS (Focal Fossa) cloud image. As the name implies, this image is pre-configured for deployment on OpenStack.

Our plan is outlined as follows: The edge node is the only OpenStack instance that can be accessed by anyone from the outside world. This instance will handle all incoming requests. Requests on the domain http://wacc.antit.tech are translated to the IP address of the edge node through an A record. 

The instances which are part of the cluster, cannot be accessed directly. Through HAproxy, the requests are forwarded to one of the available microk8s nodes. The service that is returned is determined by the port that is requested. For example, an incoming request to http://wacc.antit.tech (port 80 for HTTP) should serve the frontend of our project. The request is translated in HAProxy to port 30000 and is then forwarded through the internal network to one of available the microk8s node IPs. 
Internally, Kubernetes services are exposed using the NodePort feature. This means that all nodes listen on a specified static port that maps to the service. Nodes that do not host the pods which correspond to the service will forward the request to a node that does host the pod. 


This setup also makes it possible to use the HAproxy endpoint as SSL termination point. SSL certificates were obtained through Let's Encrypt (https://letsencrypt.org/), however, due to CORS, https does not seem to cooperate well with requests from the frontend to a different port such as the API port. For this reason, the SSL termination was not applied yet.

```
                                                                   +----------------------------------+
                                                                   |    Kubernetes microk8s cluster   |
                                                     round         |                                  |
                                                     robin         |                                  |
                                                     with          |               internal ip's      |
                                                     availability  |           +----------------+     |
                                                     check         |           |     Node 1     |     |
                                                                   |           |                |     |
                                                               +-------------->+:30000 frontend +--+  |
                                                               |   |           |:30001 api      |  |  |
http://wacc.antit.tech:80    http       public ip              |   |           |:30002 socket   |  |  |
                      :30001 api     +--------------+          |   |           |:30004 mqtt     |  |  |
                      :30002 socket  |              |          |   |           +----------------+  |  |
                      :30004 mqtt    |              |          |   |                               |  |
                                     |    HAproxy   |          |   |            +---------------+  |  |
            +----------------------> |    edge      +-------------------------->+    Node 2     |  |  |
                                     |    node      |          |   |            |             <----+  |
                                     |              |          |   |            |               |  |  |
                                     |              |          |   |            |               |  |  |
                                     +--------------+          |   |            |               |  |  |
                                                               |   |            +---------------+  |  |
                                                               |   |                               |  |
                                                               |   |            +---------------+  |  |
                                                               |                |    Node 3     |  |  |
                                                               |   +            |               |  |  |
                                                               +--------------->+              <---+  |
                                                                   |            |               |     |
                                                                   |            |               |     |
                                                                   |            +---------------+     |
                                                                   |                                  |
                                                                   +----------------------------------+

```

## Frontend
### VueJS

[//]: # (what, why)
Since we all had some experience with VueJS, we chose to use this framework for our frontend. Vue is a reactive framework, which is perfect for our single page application. In VueJS, it is also very easy to create custom reusable components.

[//]: # (how)


### NuxtJS

[//]: # (what, why)
We chose NuxtJS to create the frontend of our single-page web application. NuxtJS improves on the VueJS framework, which makes creating apps even easier. For example, it has a better default project structure and simplifies routing through different pages. It gives you more control on where to render your component, either client or server-side. This could potentially decrease load times when correctly using Server-Side Rendering (SSR). Nuxt comes bundled with Babel which handles compiling the latest Javascript versions so that also older browsers can use our application. Welcome Internet Explorer users!

[//]: # (how)
Our application consists of 6 different pages: Login, Index (home screen), Devices, Status, Users and Datastream. A user starts at the Login screen, and after logging in, is forwarded to the Index. The other pages can be reached using the menu on the left side. NuxtJS automatically creates routes for these different pages. Switching to these routes by using the menu, will not refresh the page; it is a single page application.

### Auth/JWT

[//]: # (what, why)
JSON Web Tokens are tokens which can be generated and can be granted to users, to allow/enable secure usage of the web application. They are an industry standard and work conveniently in NodeJS.

[//]: # (how)
When a user is registered in the user MongoDB, he can log in. He receives a JWT which will enable the user to send requests to the application until the token expires. We chose to use JSON Web tokens because they are easy to implement with some existing modules built for Vue. We therefore do not have to write the logic for session management our own. 



### UI/Bootstrap-Vue

[//]: # (what, why)
Bootstrap provides us with many basic page elements, with a nice looking style. Its grid system facilitates the design layout of our webpages. It allows us to quickly build the UI of our application as all components are already there. Which gives us more time to focus on what's more important in this project: the underlying architecture. 

[//]: # (how)
We use different Bootstrap components in our application. For example, the cards, buttons, and the many different tables throughout the project. To plot the graph, we use ChartJS. It's flexible, easy to use and facilitates a quick and clear presentation of data.





## Backend
### MongoDB

[//]: # (what, why)
MongoDB is a non-SQL database, which uses JSON to describe its objects. It has strong consistency and high partition tolerance. 

[//]: # (how)
We use MongoDB to keep track of the information of different users. It keeps track of the username, email, password and JWT token. We have another database in Mongo that keeps track of the devices. It stores the device name, type, subtype, status and whether is it favourite or not.

In our high availability MongoDB setup, we have three MongoDB instances. One primary member, which receives write operations, and two secondary members. The secondary members maintain a copy of the primary’s data set, by keeping track of the operations log from the primary and applying the operations asynchronously. In case the primary becomes inaccessible, the secondaries vote to elect a new primary member. 
![](https://docs.mongodb.com/manual/_images/replica-set-read-write-operations-primary.bakedsvg.svg)
(image from https://docs.mongodb.com/manual/core/replica-set-members/)
### InfluxDB

[//]: # (what, why)
We use InfluxDB to keep track of the sensor data. We chose InfluxDB because it is a time-series database. This matches our needs since we want to keep track of sensor data of specific time points. Besides, groups of data can quickly be retrieved within a certain time interval.

[//]: # (how)
All the sensor data is stored in Influxdb by time stamp, with a sensor id, type and subtype. When on the home-page, a graph shows the sensor data of the last half hour, retrieved from the InfluxDB. When a new value is added, the
frontend is notified, to update the graph. Unfortunately, InfluxDB cannot be replicated in Kubernetes without an Enterprise license. We only noticed this after working over and over again to try to make it replicate. Unfortunately, we had no more time to move to another time-series database, replicate it in Kubernetes and adapt our application logic. If we had known this beforehand we would not have used Influx. As an alternative, we could move to Prometheus, or OpenTSBD instead.



### ExpressJS
[//]: # (what, why)
ExpressJS is the most famous NodeJS framework. Its flexible, minimal and is easily scalable. We picked this framework since most of the team already had some experience with it and because it is really easy to learn because of its minimalistic approach.

[//]: # (how)
We used Express to quickly develop a RESTful API using routers. This results in a modular approach in which we can simply write a new router for each new service.

## Devices

In the system, external devices such as sensors for outside temperature and inside temperature or actuators such as air conditioning and heaters are controlled and monitored. Sensor readings are visualised on the dashboard of the frontend including but also the state of an actuator so that the user knows if his heater is on or off. The user can manually change the state of each actuator.
 
In this project, we simulate such devices by running them in a docker container running a Node instance. All communication between the device and the backend goes through an MQTT message broker. MQTT is often used for IoT devices since the message queues are perfect for communicating sensor values. The power of MQTT is that the developer is free to choose what payload or message to send to a topic. We use a bidirectional Remote Procedure Call (RPC) mechanism over MQTT. In essence, the MQTT topic structure is used as transport for the request and response strategy. A topic is still used for all sensor values but registering devices on the backend and checking device states from the backend is done using the RPC. RPC allows for API route like behaviours. We chose to use RPC instead of classical topics because we then did not have to write the logic for checking devices states on topics. RPC just made it way easier. In the background, the RPC module that we use just creates device specific topics for each function. It was nice not to invent the wheel.

A device can be of type actuator or sensor. Actuators can be of the subtype Heater or AC. Sensors can be of the subtype outside temperature or inside temperature. Type and subtype have to be specified on the construction of the device. On construction, the device will keep calling the RPC on the backend to register itself as a new device and to get an ID. Upon receiving an ID the device itself registers two RPC calls. One getter for his state and one RPC for toggling this state remotely. If the device is a sensor it will send a random numeric value to the backend every 20 seconds by calling the RPC of the backend. 

## Deployment


### Environment files

[//]: # (what, why)
When building the application with docker-compose, the developer can pass along environment variables. These variables, such as URIs and ports, enable the different components to find each other over the shared network.

[//]: # (how)
In our environment file, we define connection parameters for the MongoDB and InfluxDB. We also declare variables for the frontend configuration and state the port on which the API will be served. Depending on the production or development phase we can use different environment files to always have the correct configuration. 


## Communication

### Axios

[//]: # (what, why)
We use Axios to send requests to different components of our application. It can be easily used in JS to send HTTP requests and automates transformation for JSON data.

[//]: # (how)
Axios is mainly used in the frontend to access the backend through API requests. For example, to call requests to the MongoDB or the InfluxDB. It is also used in the backend itself, to obtain the current weather predictions of openweathermap.org. We also use Axios in the CI, when performing some initialization for functionality tests.
With all requests, we enabled the Cross-Origin Resource Sharing, which makes sure that not only requests from the same origin will be executed.


### SocketIO
We use SocketIO on the frontend and backend for WebSocket communication. We use WebSockets to quickly notify all connected clients of events. Events such as a new device are connected, a device is deleted (when it's offline), a new sensor value or an update of the current online or offline status of the backend.  This way all clients do not have to keep polling the databases and they know when to update the client state (Vuex store). The actuators are automatically turned on and off by the backend to simulate the control system of the temperature. All clients can very easily be notified of this action by the use of a socket message. 




### Mosquitto

[//]: # (what, why)
To enable communication between the devices (sensors, actuators) and the backend, we implemented communication with the message protocol MQTT. MQTT is often used for small microcontrollers, as it is designed for the IoT. As the MQTT broker, we decided to use Mosquitto, since it is very lightweight. No setup is required for the MQTT broker. 

[//]: # (how)
The broker hosts the topics on which the devices and backend can subscribe and publish on/to. Sensors publish sensor readings on a topic. The backend listens for these values and records them in the Influx database. Both the devices and the backend use the Remote Procedure Call mechanism. More about that in the Devices section. 

## CI
### GitLab
We wanted to use an external CI platform such as CircleCI but unfortunately, we did not get authorization from the owners of the rug-wacc repository on Github. To circumvent this we set up a new repo on Gitlab and used this as a second remote. Since GitLab already has built-in CI/CD we did not use a platform such as CircleCI. 

[//]: # (what, why)
GitLab is a development platform with an easy-to-use Web IDE. It covers the whole development and operations cycle. It has built-in continuous integration and delivery features, which are convenient when pushing updates while developing. We set up a CI/CD pipeline on Gitlab in which on each push to the development branch the backend would be tested, the backend and frontend would be built as Docker images and finally be pushed to the Docker Hub. The limited free CI/CD minutes on Gitlab were not enough for all our tests. We moved the Gitlab runner to our Kubernetes Cluster to increase build times and testing but most importantly because we are now not limited any longer. 



### Mocha

[//]: # (what, why)
Mocha is a simple and flexible javascript testing framework. It serially runs tests to audit functionality.

[//]: # (how)
Whenever we push updates to GitLab, we test if the backend behaves as it is supposed to. To check this, we wrote a series of test files, which are divided by the used routes. When testing the backend, we do not need all other components. Therefore, we use a separate docker configuration file to build a testing environment. To perform assertions, we use the Chai assertion library. 

## Fault tolerance
The user of the application has to know if the backend is offline. When the backend is offline the user can't do anything and will run into problems. So we have to notify the user. Since we already used sockets in our application we used this to check the status of the backend. Whenever the socket connection is lost we can assume that the backend is offline. The user is then notified in the application by means of an alert. In the login page, we disable the button that is used to submit form data and login. Here we also show a message that the backend is offline. 





# Setting up the project
## Requirements:
- Docker & Docker Compose
- Node & npm

optional:
- kubernetes

## Install instructions
Run and build the containers
```
docker-compose up
```

Now you should be able to connect to http://localhost:3000


## Testing backend
```
# First startup mongo, influx, backend and mqtt
docker-compose -f docker-compose.test.yml up

# When all services are started run the tests
# Note: the name 'controlsystem' depends on the name of the parent directory.
docker exec controlsystem_backend_1 mocha --timeout 2000

```


## Building docker images and pushing to Docker HUb
```
cd ./backend
docker build -t wcccontrolsystem/wcccontrolsystem:backend -f Dockerfile.production .
docker push wcccontrolsystem/wcccontrolsystem:backend

cd ./frontend
docker build -t wcccontrolsystem/wcccontrolsystem:frontend -f Dockerfile.production .
docker push wcccontrolsystem/wcccontrolsystem:frontend
```
## Kubernetes deployment
go to the `./deployment` folder and run the following commands

```
helm repo add bitnami https://charts.bitnami.com/bitnami
```
### Install MongoDB
```
helm install mongodb --values mongo-values.yml bitnami/mongodb
```
### Install Influx DB
```
helm repo add influxdata https://helm.influxdata.com/
helm install influxdb --values influx-values.yml influxdata/influxdb
```
### Install Mosquitto
```
helm repo add k8s-at-home https://k8s-at-home.com/charts/
helm install mosquitto --values mongo-values.yml k8s-at-home/mosquitto

```
### Deployment
Create docker secrets to pull the image from private repository
```
create secret docker-registry regcred --docker-username=[myusername] --docker-password=[mypassword] --docker-email=[me]@gmail.com --docker-server=https://index.docker.io/v1/
```

Apply the configurations
```
kubectl apply -k .
kubectl apply -f mosquitto-service.yml
```

