
require('dotenv').config()
const MQTT = require("mqtt")
const RPC = require("mqtt-json-rpc")

/**
 ** Represents a simulated device such as a temperature sensor or a heater.
 * Connects to the backend with RPC calls through a MQTT broker.
 */
class Device {
    constructor(type, subtype) {
        console.log(`Device: ${type} - ${subtype}: Online and connecting with MQTT service`)
        this.type = type
        this.subtype = subtype
        // connect with broker
        this.mqtt = MQTT.connect(process.env.MQTT_URI, {
            keepalive: 180,
        })
        let tries = 1
        this.rpc = new RPC(this.mqtt)
        this.deviceInfo = null
        this.state = (type === 'Sensor')  // Sensors are always 'on'
        this.latest_value = 12

        this.rpc.on("connect", () => {
            console.log(`Device: ${type} - ${subtype}: Connected.`)
            // Continuously try to fetch an ID from the backend.
            let interval = setInterval(() => {
                tries += 1
                if (this.deviceInfo != null) {
                    clearInterval(interval)
                    // register RPC calls
                    this.registerStateToggle()
                    this.registerGetState()
                    if (this.type === 'Sensor') {  // Only sensors send sensor readings.
                        setInterval(() => {
                            // simulate sensor readings by sending random values
                            this.sendRandomValue()
                        }, 1000 * 20)
                    }
                } else {
                    this.getDeviceInfo()  // fetch id
                }
            }, 800)
        })

    }

    /**
     * Register RPC call for fetching the state of this device
     */
    registerGetState() {
        if (this.deviceInfo == null) throw Error('No ID present.')
        let rpcRoute = 'devices/' + this.deviceInfo._id + '/state'
        console.log('Registering RPC route:', rpcRoute)
        this.rpc.register(rpcRoute, () => {
            return this.state
        })
    }

    /**
     * Register RPC call for toggling the state of this device and returning new state.
     */
    registerStateToggle() {
        if (this.deviceInfo == null) throw Error('No ID present.')
        this.rpc.register('devices/' + this.deviceInfo._id + '/state/toggle', () => {
            this.state = !this.state
            return this.state
        })

    }

    /**
     * Simulate a sensor by sending a random value
     */
    sendRandomValue() {
        if (this.rpc.connected) {
            if (this.deviceInfo != null && this.deviceInfo._id != null) {
                console.log('Sending sensor readings to broker')
                let newValue = this.latest_value + this.getRandomArbitrary(-2, 2)
                this.latest_value = (newValue > 10 && newValue < 14) ? newValue : this.latest_value
                const data = {
                    sensor_id: this.deviceInfo._id,
                    value: this.latest_value
                }
                this.rpc.publish('devices/data', JSON.stringify(data))
            } else {
                this.deviceInfo = this.getDeviceInfo()
            }
        }
    }

    /**
     * Connect with the backend through an RPC call to fetch an ID.
     * @returns {Promise<void>}
     */
    async getDeviceInfo() {
        await this.rpc.call("devices/new", this.type, this.subtype).then((response) => {
            // console.log("devices/new response: ", response)
            // Response will be a json object of the newly created device including _id
            this.deviceInfo = response
            return response
        }).catch(err => {
            // console.error(`Error on connecting with RPC route devices/new: ${err}`)
            return null
        })
    }

    getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }
}

// Construct 4 simulated devices.
let actuatorAC = new Device('Actuator', 'AC')
let actuatorHeater = new Device('Actuator', 'Heater')
let temperatureSensor = new Device('Sensor', 'Temperature_Inside')
let humiditySensor = new Device('Sensor', 'Temperature_Outside')
