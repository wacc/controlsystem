require('dotenv').config()
const Influx = require("influx")
const mongoose = require('mongoose')


/**
 * Connect to MongoDB
 * @returns {Promise<void>}
 */
const connect = async () => {
    try {

        let url = "mongodb://";
        if (process.env.MONGODB_USER !== "" && process.env.MONGODB_PASSWORD !== "")
            url += process.env.MONGODB_USER + ":" + process.env.MONGODB_PASSWORD + "@"
        url += process.env.MONGODB_HOST + "/" + process.env.MONGODB_DATABASE

    await mongoose.connect(url, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
      useFindAndModify: true,
      autoReconnect: true,
    }).then(() => {
      require('../seeder')
    })
  } catch (err) {
    console.error(err)
    console.log('MongoDB connection error. Please make sure MongoDB is running.')
    process.exit()
  }
}
connect();

/**
 * Connect to Influx read
 * @type {Influx.InfluxDB}
 */
const influx_read = new Influx.InfluxDB({
    host: process.env.INFLUXDB_URI_READ,
    database: 'devices',
    user: process.env.INFLUXDB_USER,
    password: process.env.INFLUXDB_PASS,
    schema: [
        {
            measurement: 'temperature',
            fields: {
                sensor_id: Influx.FieldType.STRING,
                value: Influx.FieldType.FLOAT
            },
            tags: ['sensor']
        }
    ]
})

/**
 * Connect to Influx write
 * @type {Influx.InfluxDB}
 */
const influx_write = new Influx.InfluxDB({
    host: process.env.INFLUXDB_URI_WRITE,
    database: 'devices',
    user: process.env.INFLUXDB_USER,
    password: process.env.INFLUXDB_PASS,
    schema: [
        {
            measurement: 'temperature',
            fields: {
                sensor_id: Influx.FieldType.STRING,
                value: Influx.FieldType.FLOAT
            },
            tags: ['sensor']
        }
    ]
})

module.exports = { influx_read, influx_write }

