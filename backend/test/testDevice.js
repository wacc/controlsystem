const chai = require('chai');
const chaiHttp = require('chai-http');
const axios = require('axios')
const test_user = require('./test_user')
const should = chai.should();
chai.use(chaiHttp);

let test_device

describe('Test Devices', () => {
    before('Login user', (done) => {
        axios.post('http://backend:3001/api/auth/login', test_user.TEST_USER)
            .then(res => {
                test_user.token = res.data.token
                done()
            }).catch(err => console.log(err))
    })
    describe('New device', () => {
        it('it should be able to make a new device', (done) => {
            chai
                .request('http://backend:3001')
                .post('/api/devices')
                .set({Authorization: `Bearer ${test_user.token}`})
                .send({
                    name: 'test_device',
                    type: 'test_type',
                    subtype: 'test_subtype',
                    status: true,
                    value: 1,
                    favorite: true,
                })
                .end((err, res) => {
                    res.should.have.status(201)
                    res.should.be.json
                    done()
                })
        })
        it('it should NOT be able to make a device with illegal object',
            (done) => {
                chai
                    .request('http://backend:3001')
                    .post('/api/devices')
                    .set({Authorization: `Bearer ${test_user.token}`})
                    .send({
                        test: 'test',
                        illegal_property: 'illegal property',
                    })
                    .end((err, res) => {
                        res.should.have.status(400)
                        res.should.be.json
                        res.should.have.property("error")
                        done()
                    })
            })
    })
    describe('Get all devices', () => {
        it('it should be able to get devices', ((done) => {
            chai
                .request('http://backend:3001')
                .get('/api/devices')
                .set({Authorization: `Bearer ${test_user.token}`})
                .end((err, res) => {
                    res.should.have.status(201)
                    res.should.be.json
                    res.should.have.property('body')
                    res.body.should.have.property('devices')
                    res.body.devices.should.an('array')
                    res.body.devices.should.have.lengthOf(1)
                    res.body.devices[0].should.have.property('name')
                    res.body.devices[0].name.should.equal('test_device')
                    test_device = res.body.devices[0]
                    done()
                })
        }))
    })
    // describe('Toggle status', () => {
    //     //TODO fix multiple status codes (201, 500)
    //     it('it should be able to toggle the status of a device', ((done) => {
    //         chai
    //             .request('http://backend:3001')
    //             .post('/api/devices/toggleStatus')
    //             .set({ Authorization: `Bearer ${test_user.token}`})
    //             .send(test_device)
    //             .end((err, res) => {
    //                 res.should.have.status(201)
    //                 res.should.be.json
    //                 res.should.have.property("body")
    //                 res.body.should.have.property('newState')
    //                 console.log('newState: ', res.body.newState)
    //                 done()
    //             })
    //     }))
    // })
    describe('Favorites', () => {
        describe('Get Favorite devices', () => {
            it('it should return the favorite devices', ((done) => {
                chai
                    .request('http://backend:3001')
                    .get('/api/devices/favorite')
                    .set({Authorization: `Bearer ${test_user.token}`})
                    .end((err, res) => {
                        res.should.have.status(201)
                        res.should.be.json
                        res.should.have.property("body")
                        res.body.should.have.property('favorite_devices')
                        res.body.favorite_devices.should.be.a('array')
                        res.body.favorite_devices.should.have.lengthOf(1)
                        res.body.favorite_devices[0].should.have.property('name')
                        res.body.favorite_devices[0].name.should.equal('test_device')
                        done()
                    })
            }))
        })
        describe('Set Favorite', () => {
            it('it should update the status of a specified device', ((done) => {
                test_device.favorite = false //Change it locally to false
                chai
                    .request('http://backend:3001')
                    .post('/api/devices/update')
                    .set({Authorization: `Bearer ${test_user.token}`})
                    .send(test_device)
                    .end((err, res) => {
                        res.should.have.status(201)
                    })
                chai
                    .request('http://backend:3001')
                    .get('/api/devices')
                    .set({Authorization: `Bearer ${test_user.token}`})
                    .end((err, res) => {
                        res.should.have.status(201)
                        res.should.be.json
                        res.should.have.property('body')
                        res.body.should.have.property('devices')
                        res.body.devices.should.an('array')
                        res.body.devices.should.have.lengthOf(1)
                        res.body.devices[0].should.have.property('name')
                        res.body.devices[0].name.should.equal('test_device')
                        res.body.devices[0].should.have.property('favorite')
                        res.body.devices[0].favorite.should.equal(false) //Check if it is set to false
                        done()
                    })
            }))
        })
        describe('Set Name', () => {
            it('should be able to update the name', ((done) => {
                test_device.name = 'changed_name'
                chai
                    .request('http://backend:3001')
                    .post('/api/devices/update/name')
                    .set({Authorization: `Bearer ${test_user.token}`})
                    .send(test_device)
                    .end((err, res) => {
                        res.should.have.status(201)
                    })
                chai
                    .request('http://backend:3001')
                    .get('/api/devices')
                    .set({Authorization: `Bearer ${test_user.token}`})
                    .end((err, res) => {
                        res.should.have.status(201)
                        res.should.be.json
                        res.should.have.property('body')
                        res.body.should.have.property('devices')
                        res.body.devices.should.an('array')
                        res.body.devices.should.have.lengthOf(1)
                        res.body.devices[0].should.have.property('name')
                        res.body.devices[0].name.should.equal('changed_name')
                        done()
                    })
            }))
        })
    })
    after('After, Logout',(done) => {
        axios.post('http://backend:3001/api/auth/logout', {},{headers: {Authorization: `Bearer ${test_user.token}`}})
            .then(res => {
                done()
            }).catch(err => console.log(err))
    })
})
