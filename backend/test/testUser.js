const chai = require('chai');
const chaiHttp = require('chai-http');
const axios = require('axios')
const test_user = require('./test_user')
const should = chai.should();
chai.use(chaiHttp);

describe('Test Users', () => {
    before('Login user', (done) => {
        axios.post('http://backend:3001/api/auth/login', test_user.TEST_USER)
            .then(res => {
                test_user.token = res.data.token
                done()
            }).catch(err => console.log(err))
    })
    describe('Obtaining users information', () => {
        it('It should return all the users name and email', ((done) => {
            chai
                .request('http://backend:3001')
                .get('/api/users')
                .set({Authorization: `Bearer ${test_user.token}`})
                .end((err, res) => {
                    res.should.have.status(201)
                    res.should.be.json
                    res.should.have.property('body')
                    res.body.should.have.property('users')
                    res.body.users.should.be.an('array')
                    res.body.users.should.have.lengthOf(2)
                    let user = res.body.users[1]
                    user.should.have.property('name')
                    user.name.should.equal('test_user')
                    user.should.have.property('email')
                    user.email.should.equal('test_user@testing.com')
                    done()
                })
        }))
    })
    after('After, Logout',(done) => {
        axios.post('http://backend:3001/api/auth/logout', {},{headers: {Authorization: `Bearer ${test_user.token}`}})
            .then(res => {
                done()
            }).catch(err => console.log(err))
    })
})
