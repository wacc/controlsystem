//Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
chai.use(chaiHttp);
const test_user = require('./test_user')


describe('Users', () => {
    describe('Create user', () => {
        console.log("test_user", test_user)
        it('user being created', (done) => {
            //Creating a user to gain authentication
            chai
                .request('http://backend:3001')
                .post('/api/auth')
                .send(test_user.TEST_USER)
                .end((err, res) => {
                    res.should.have.status(201)
                    res.should.be.json
                    res.body.should.be.a("object")
                    res.body.should.have.property("token")
                    done()
                })
        })
    })
    describe('Login', () => {
        it('it should login an existing user', (done) => {
            chai
                .request('http://backend:3001')
                .post('/api/auth/login')
                .send(test_user.TEST_USER)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.should.be.json
                    res.body.should.be.a("object")
                    res.body.should.have.property("token")
                    test_user.token = res.body.token
                    done()
                })

        })
        it('it should not login illegal user', (done) => {
            chai
                .request('http://backend:3001')
                .post('/api/auth/login')
                .send({email: "incorrect_name", password: "incorrect_password"})
                .end((err, res) => {
                    res.should.have.status(401)
                    res.should.have.property("error")
                    done()
                })
        })
    })
    describe('Get current users info', () => {
        it('it should get the currents users info with correctJWT', (done) => {
            chai
                .request('http://backend:3001')
                .get('/api/auth/user')
                .set({ Authorization: `Bearer ${test_user.token}`})
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.a("object")
                    res.body.should.have.property("user")
                    // res.body.should.have.property("email")
                    done()
                })
        })
        it('it should NOT get the currents users info without JWT', (done) => {
            chai
                .request('http://backend:3001')
                .get('/api/auth/user')
                .set({ Authorization: `Bearer asdfbwetwer`})
                .end((err, res) => {
                    res.should.have.status(401)
                    res.should.have.property("error")
                    done()
                })
        })
    })
    describe('Logout', () => {
        it('it should log out the user', (done) =>{
            chai
                .request('http://backend:3001')
                .post('/api/auth/logout')
                .set({ Authorization: `Bearer ${test_user.token}`})
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
})
