const chai = require('chai');
const chaiHttp = require('chai-http');
const axios = require('axios')
const test_user = require('./test_user')
const should = chai.should();
chai.use(chaiHttp);

describe('Test sensors', () => {
    describe('Get sensor data', () => {
        it('It should return the sensor  data ', ((done) => {
            chai
                .request('http://backend:3001')
                .get('/api/temperature')
                .set({Authorization: `Bearer ${test_user.token}`})
                .end((err, res) => {
                    res.should.have.status(201)
                    res.should.be.json
                    res.should.have.property('body')
                    //TODO check on contents
                    console.log('res.body:', res.body)
                    done()
                })
        }))
    })

})
