const chai = require('chai');
const chaiHttp = require('chai-http');
const axios = require('axios')
const test_user = require('./test_user')
const should = chai.should();
chai.use(chaiHttp);

describe('Test openweathermap weather data', () => {
    before('Login user', (done) => {
        axios.post('http://backend:3001/api/auth/login', test_user.TEST_USER)
            .then(res => {
                test_user.token = res.data.token
                done()
            }).catch(err => console.log(err))
    })
    describe('Get current weather data', () => {
        it('It should return the current weather data ' +
            'from openweathermap.org through the back-end', ((done) => {
            chai
                .request('http://backend:3001')
                .get('/api/weather/current')
                .set({Authorization: `Bearer ${test_user.token}`})
                .end((err, res) => {
                    res.should.have.status(201)
                    res.should.be.json
                    res.should.have.property('body')
                    res.body.should.be.an('object')
                    res.body.should.have.property('current')
                    res.body.current.should.be.an('object')
                    done()
                })
        }))
    })
    after('After, Logout',(done) => {
        axios.post('http://backend:3001/api/auth/logout', {},{headers: {Authorization: `Bearer ${test_user.token}`}})
            .then(res => {
                done()
            }).catch(err => console.log(err))
    })
})
