const MQTT = require('mqtt')
const RPC = require('mqtt-json-rpc')
const axios = require('axios')
var {nanoid} = require('nanoid')
const {influx_read, influx_write} = require('./db/db')
const Device = require('./models/Device')

const mqtt = MQTT.connect(process.env.MQTT_URI, {keepalive: 180})
const rpc = new RPC(mqtt, {timeout: 1000})

let message_counter = 0 //Every 5 messages, update the actuators. Every 300 messages, update the outside temperature
let outsideTemp = null //

module.exports = function (io) {

    /**
     * Connect to MQTT broker
     */
    rpc.on('connect', () => {
        console.log('Connected to MQTT Broker')

        // Register a RPC to handle new sensors and actuators
        rpc.register("devices/new", async (type, subtype) => {
            const deviceInfo = {
                type: type,
                subtype: subtype,
                name: type + '-' + subtype + '-' + nanoid(10),
                favorite: false
            }
            const device = new Device(deviceInfo)
            await device.save()
            // notify all clients of new devices
            io.emit('new device')
            return device

        })
        rpc.subscribe('devices/data')  // handle sensor data
        updateDeviceConnections()  // update device alive status once
        setInterval(() => updateDeviceConnections(), 1000 * 10)  // and every 10s
    })

    rpc.on('message', (topic, payload) => {
        const message = JSON.parse(payload.toString())
        if (topic === 'devices/data') return handleData(message)
    })

    /**
     * Poll all known devices to see if they are still alive. Emit socket message on change.
     * @returns {Promise<void>}
     */
    async function updateDeviceConnections() {
        const devices = await Device.find()
        for (let device of devices) {
            let state = await getDeviceState(device._id)
            if (state === null) {
                console.log('Removing ', device._id, ' since it is not alive')
                // remove device from mongoose
                await Device.findByIdAndDelete(device._id)
                // notify frontend of deleted device
                io.emit('device deleted', device._id)
            } else {
                await Device.findOneAndUpdate({_id: device._id}, {status: state})
                // notify frontend of state update
                io.emit('device state update', device._id, state)
            }
        }
    }

    /**
     * Toggle the state of a device by using an RPC.
     * @param _id
     * @returns {Promise<T>}
     */
    function toggleDeviceStatus(_id) {
        if (!rpc.connected) {
            throw new Error('Not connected to RPC')
        }
        return rpc.call('devices/' + _id + '/state/toggle')
            .then((response) => {
                return response
            })
            .catch(() => {
                return null
            })
    }


    /**
     * Fetch the current state of the device by using a RPC
     * @param _id
     * @returns {Promise<T>}
     */
    function getDeviceState(_id) {
        if (!rpc.connected) {
            throw new Error('Not connected to RPC')
        }
        return rpc.call('devices/' + _id + '/state')
            .then((response) => {  // alive
                return response
            }).catch(() => {  // not alive
                return null
            })
    }

    /**
     * Write sensor values to influxDB
     * @param message The message received
     */
    function writeToInflux(message) {
        console.log(`writing message: ${message.sensor_id} and ${message.value}`)
        influx_write.writePoints([
            {
                measurement: 'temperature',
                fields: {
                    sensor_id: message.sensor_id,
                    value: message.value,
                }
            }
        ]).catch(err => {
            console.error(`Error saving to InfluxDB. Check connection. ${err}`)
        }).then(() => {
            // Notify user of new sensor value
            influx_read.query("select * from temperature " +
                "where sensor_id=\'" + message.sensor_id + "\' " +
                "order by time desc limit 1"
            ).then(result => {
                let row = result[0]
                io.emit('sensor value', {sensor_id: row.sensor_id, value: row.value, time: row.time._nanoISO})
            }).catch(err => console.log(err.message))
        })
    }

    /**
     * Handle the data from an incoming message
     * @param message
     */
    function handleData(message) {
        writeToInflux(message)
        updateOutsideTemp()
        updateActuatorStatus(message)
    }

    /**
     * Update the outside temperature every 300 messages
     * @returns {Promise<void>}
     */
    async function updateOutsideTemp() {
        if (outsideTemp != null || message_counter >= 300) {
            message_counter = 0
            try {
                outsideTemp = await axios.get("https://api.openweathermap.org/data/2.5/onecall?lat=53.2194&lon=6.5665&units=metric&exclude=minutely,hourly,daily&appid=fd3150a661c1ddc90d3aefdec0400de4")
            } catch (error) {
                console.log('Unable to update actuators with weather data')
                console.log(error)
            }
        }
    }

    /**
     * Update the actuator status by comparing sensor data with outside temperature
     * @param message
     * @returns {Promise<void>}
     */
    async function updateActuatorStatus(message) {
        message_counter++
        if (message_counter % 5 === 0) {
            if (outsideTemp != null && message.value > outsideTemp) {
                //activate all ACs, deactivate Heaters
                updateStatus('AC', true, message.value)
                updateStatus('Heater', false, message.value)
            } else {
                //deactivate all ACs, activate Heaters
                updateStatus('AC', false, message.value)
                updateStatus('Heater', true, message.value)
            }
        }
    }

    /**
     * Update the status of an actuator
     * @param subtype
     * @param activate
     * @param value
     */
    function updateStatus(subtype, activate, value) {
        let changes = false
        getAllDevicesOfSubtype(subtype).then(devices => {
            for (const device of devices) {
                if (device.status !== activate) { //TODO request status with mqtt from Devices themselves
                    device.status = activate
                    changes = true
                    toggleDeviceStatus(device._id).then(status => {
                        console.log(device.name, ' status set to: ', status)
                    }).catch(err => console.log(err))
                }
            }
            if (changes){
                io.emit('Devices activation', {'value': value, 'type': subtype, 'activation': activate})
            }
        })
    }

    /**
     * Obtain a list of all the devices of a certain subtype from the MongoDB
     * @param subtype
     * @returns {Promise<*>}
     */
    async function getAllDevicesOfSubtype(subtype) {
        try {
            return await Device.find({subtype: subtype})
        } catch (error) {
            console.log(error.err)
        }
    }

    module.exports.toggleDeviceStatus = toggleDeviceStatus
}
