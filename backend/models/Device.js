const mongoose = require('mongoose')

const deviceSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    // actuators, sensors
    type: {
        type: String,
        required: true
    },
    // AC, Heater, Temperature Outside, Temperature Inside
    subtype: {
        type: String,
        required: true
    },
    status: Boolean,
    value: Number,
    favorite: {
        type: Boolean,
        required: true
    }
})

const Device = mongoose.model('Device', deviceSchema)
module.exports = Device
