require('dotenv').config()
const express = require('express')
const jwt = require('express-jwt')
const app = express()
// eslint-disable-next-line import/order
const server = require('http').createServer(app)
const io = require('socket.io')(4113)
const cors = require('cors')

app.use(cors())

const { influx_read, influx_write } = require('./db/db')

// Routes
const usersRouter = require('./routers/user')
const authRouter = require('./routers/auth')
const devicesRouter = require('./routers/device')
const sensorDataRouter = require('./routers/sensorData')
const weatherDataRouter = require('./routers/weatherData')
const k8sRouter = require('./routers/k8sStatus')
require('./db/db')
require('./mqtt')(io)

console.log(process.env)

// Use JSON web tokens
app.use(jwt({
  secret: process.env.JWT_KEY,
  algorithms: ['HS256']
}).unless({ path: ['/api/auth','/api/auth/login'] }))

app.use(express.json())
app.use(usersRouter)
app.use(authRouter)
app.use(devicesRouter)
app.use(sensorDataRouter)
app.use(weatherDataRouter)
app.use(k8sRouter)

// Create database for sensor data if it does not exist yet
influx_read.getDatabaseNames()
    .then(names => {
      console.log('INFLUX got following databases:\n' + names)
      if (!names.includes('devices')) {
        console.log('Influxdb init script did not run successfully. No database created. Creating database now.')
        return influx_read.createDatabase('devices');
      }
    })
    .catch(err => {
      console.error(`Error creating Influx database!`);
    })

// Export express app
module.exports = app

if (require.main === module) {
  const port = process.env.PORT || 3001
  server.listen(port, () => {
    console.log(`API server listening on port ${port}`)
  })
}

