const express = require('express')
const router = express.Router()
const axios = require('axios')
/**
    Get open weather map forecast data
 */
router.get('/api/weather/current', async (req, res) => {
    // 53.2194° N, 6.5665° E --- Groningen coordinates (Changes to Amsterdam)
    // Weather data for last couple of days, every 3 hours:
    // axios.get("http://api.openweathermap.org/data/2.5/forecast?q=Groningen&units=metric&appid=fd3150a661c1ddc90d3aefdec0400de4").
    axios.get("https://api.openweathermap.org/data/2.5/onecall?lat=53.2194&lon=6.5665&units=metric&exclude=minutely,hourly,daily&appid=fd3150a661c1ddc90d3aefdec0400de4").
        then(
            data => {
                res.status(201).send(data.data)
            }).
        catch(
                error => {
                console.log(error)
                res.status(400).send(error)
            })
})

module.exports = router
