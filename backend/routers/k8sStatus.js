const express = require('express')
const router = express.Router()
const k8s = require('@kubernetes/client-node')
const Client = require('kubernetes-client').Client

const kc = new k8s.KubeConfig()
kc.loadFromDefault()

const coreV1Api = kc.makeApiClient(k8s.CoreV1Api)
const appsV1Api = kc.makeApiClient(k8s.AppsV1Api)




const client = new Client({version: '1.13'})

/**
 * Delete a Pod
 */
router.post('/api/pod/delete', async (req, res) => {
    await coreV1Api.deleteNamespacedPod(req.body.podName, 'default')
        .then(response => res.send(response))
        .catch(err => res.status(err.statusCode).send(err))
})

router.get('/api/pod', async (req, res) => {
    await client.api.v1.namespaces('default').pods(req.body.podName).get()
        .then(response => res.send(response))
        .catch(err => res.status(503).send(err))
})

router.get('/api/events', async (req, res) => {
    await client.api.v1.events.get()
        .then(response => {
            let events = []
            response.body.items.forEach(item => {
                events.push({
                    name: item.metadata.name,
                    type: item.type,
                    message: item.message,
                    lastTimestamp: item.lastTimestamp,
                    count: item.count
                })
            })
            res.send(events)
        })
        .catch(err => res.status(503).send(err)

)
})

/**
 * Fetch all deployment names and replicas
 */
router.get('/api/deployments', async (req, res) => {
    await client.apis.apps.v1.deployments.get()
        .then((APIResponse) => {
            let deployments = []
            APIResponse.body.items.forEach(item => {
                deployments.push({
                    name: item.metadata.name,
                    replicas: item.spec.replicas
                })
            })
            res.send(deployments)
        })
        .catch((err) => res.status(503).send(err))
})

/**
 * Scale a deployment
 */
router.post('/api/deployments/scale', async (req, res) => {
    let namespace = 'default'
    const result = await appsV1Api.readNamespacedDeployment(req.body.name, namespace)
    let deployment = result.body

    deployment.spec.replicas = req.body.replicas
    await appsV1Api.replaceNamespacedDeployment(req.body.name, namespace, deployment)
        .then(result => res.send(result))
        .catch(err => res.status(503).send(err))
})


/**
 * Get the status of all running pods on k8s
 */
router.get('/api/status', async (req, res) => {
    // List all pods
    coreV1Api.listNamespacedPod('default').then(result => {
        let podsStatus = []
        let items = result.body.items;
        items.forEach(item => {
            podsStatus.push({
                name: item.metadata.name,
                app: item.metadata.labels.app,
                ready: item.status.containerStatuses.ready,
                phase: item.status.phase,
                restartCount: item.status.containerStatuses.restartCount,
                startTime: item.status.startTime,
            })
        })
        res.send(podsStatus)
    }).catch(err => {
        res.status(503).send(err)
    })
})

module.exports = router