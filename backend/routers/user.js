const express = require('express')
const router = express.Router()
const User = require('../models/User')

/**
    Get all users
 */
router.get('/api/users', async (req, res) => {
    try {
        const users = await User.find({}, {
            "_id": false,
            "name": true,
            "email": true,
        })
        res.status(201).send({ users })
    } catch (error){
        res.status(400).send(error)
    }
})


module.exports = router
