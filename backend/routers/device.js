const express = require('express')
const router = express.Router()
const Device = require('../models/Device')
const mqtt = require('../mqtt')

/**
    Make a new device
 */
router.post('/api/devices', async (req, res) => {
    try {
        const device = new Device(req.body)
        await device.save()
        res.status(201).send({ device })
    } catch (error) {
        res.status(400).send(error)
    }
})

/**
    Get all devices
 */
router.get('/api/devices', async (req, res) => {
    try {
        const devices = await Device.find()
        res.status(201).send({ devices })
    } catch (error){
        res.status(400).send(error)
    }
})

/**
 * Toggle the status of a device
 */
router.post('/api/devices/toggleStatus', async (req, res) => {
    try {
        let newState = await mqtt.toggleDeviceStatus(req.body._id)
        if (newState === null) {
            res.status(500).send(newState)
        } else {
            res.status(201).send(newState)
        }
        console.log('Toggled state of ' + req.body._id + ' to ', newState)
    } catch (err) {
        console.error('Cannot toggle state, no connection with MQTT broker')
        res.status(400).send(err)
    }
})


/**
    Get favorite Devices
 */
router.get('/api/devices/favorite', async(req, res) => {
    try {
        const favorite_devices = await Device.find({favorite: true})
        res.status(201).send({ favorite_devices })
    } catch (error){
        res.status(400).send(error)
    }
})

/**
    Change device favorite status
 */
router.post('/api/devices/update', async (req, res) => {
    try {
        await Device.findByIdAndUpdate(req.body._id, {'favorite': req.body.favorite}, function (error, result) {
            if (error) {
                res.status(400).send(error)
            } else {
                res.status(201).send()
            }
        });
    } catch(e){
        res.status(400).send(e)
    }
})

/**
 * Change the name of the device
 */
router.post('/api/devices/update/name', async (req, res) => {
    try {
        await Device.findByIdAndUpdate(req.body._id, {'name': req.body.name}, function (error, result) {
            if (error) {
                res.status(400).send(error)
            } else {
                res.status(201).send({result})
            }
        });
    } catch(e){
        res.status(400).send(e)
    }
})

module.exports = router
