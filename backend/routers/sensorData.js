const express = require('express')
const router = express.Router()
// const influx = require('../db/influx')
const { influx_read, influx_write } = require('../db/db')

/**
    Get last 30 minutes of temperature sensor readings
 */

router.get('/api/temperature', async (req, res) => {
    let id = req.query.id
    influx_read.query(" select * from \"temperature\" " +
        "where \"time\" > now() - 30m and " +
        "\"sensor_id\" = \'"+id+"\' "+
        "order by time desc"
    ).then(result => {
        res.status(201).send({result})
    }).catch(err => {
        res.status(500).send({err})
    })
})

/**
 * Write a sensor value to influx
 */
router.post('/api/temperature', async (req, res) => {
    influx_write.writePoints([
        {
            measurement: 'temperature',
            fields: {
                sensor_id: req.body.sensor_id,
                value: req.body.value
            }
        }
    ])
    .then(result => {
        res.status(201).send({result})
    })
    .catch(err => {
        console.error(`Error saving to InfluxDB. Check connection. ${err}`)
        res.status(500).send(err)
    })
})

module.exports = router
