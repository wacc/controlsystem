const express = require('express')
const User = require('../models/User')

const router = express.Router()

/**
 * Creates a new user
 */
router.post('/api/auth', async (req, res) => {
  try {
    const user = new User(req.body)
    await user.save()
    const token = await user.generateAuthToken()
    console.log('sending token')
    res.status(201).send({ token })
  } catch (error) {
    res.status(400).send(error)
  }
})

/**
 *  Login a registered user
 */
router.post('/api/auth/login', async (req, res) => {
  try {
    const { email, password } = req.body

    const user = await User.findByCredentials(email, password)
    const token = await user.generateAuthToken()
    await res.status(200).send({ token })
  } catch (error) {
    console.log(error)
    await res.status(401).send({ error: 'Login failed! Check authentication credentials' })
  }
})

/**
 *  View logged in user profile
 */
router.get('/api/auth/user', async (req, res) => {
  await res.status(200).send({ user: req.user })
})

/**
 *  Log user out of the application
 */
router.post('/api/auth/logout', async (req, res) => {
  await res.status(200).send({ status: 'OK' })
})

module.exports = router
