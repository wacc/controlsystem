require('./db/db')
const User = require('./models/User')
// Seed the db
const seed = async () => {
    const existingUsers = await User.find({email: 'admin@wcccontrolsystem.com'})
    if (existingUsers.length > 0)
        return

    try {
        const user = new User({
            name: 'Admin',
            email: 'admin@wcccontrolsystem.com',
            password: 'admin'
        })

        await user.save()
    } catch (e) {
        console.error('User already exists')
    }
}
seed();
